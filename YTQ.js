function changeQuality(quality) {
    if (document.querySelectorAll(".ytp-settings-button") == null) {
        setTimeout(changeQuality(quality), 400);
        return;
    }
    
    setTimeout(function() {
        document.querySelectorAll(".ytp-settings-button")[0].click();
        if (document.getElementsByClassName("ytp-menuitem")[document.getElementsByClassName("ytp-menuitem").length-1].innerHTML.indexOf("Quality") != -1)
        {
            document.getElementsByClassName("ytp-menuitem")[document.getElementsByClassName("ytp-menuitem").length-1].click();
            var x = document.querySelectorAll(".ytp-menuitem");
            var i = 0;
            var flag = true;
            for (var i = 0; i < x.length; i++){
                if (x[i].innerText.indexOf(quality.split(" ")[0]) != -1 && x[i].innerText.indexOf("Auto") == -1) {
                    x[i].click();
                    console.log("Setting video quality to: " + x[i].innerText);
                    flag = false;
                    changed = true;
                    break;
                }
            } 
            if (flag) {
                x[0].click();
            }
        }
    }, 1100);

}

var quality = "1080";

chrome.storage.local.get('quality', function (items) {
    if (items.quality)
        quality = items.quality;
    changeQuality(quality);
}); 