document.onreadystatechange = function () {
    if (document.readyState == "complete") {   
        chrome.storage.local.get('quality', function (q) {
            if (q.quality) {
                if (document.getElementById('quality') != null) {
                    document.getElementById('quality').value = q.quality;
                }
            }
        }); 
        if (document.getElementById('quality') != null) {
            document.getElementById('quality').addEventListener("change", function () {
                chrome.storage.local.set({
                    quality: document.getElementById('quality').value
                }, function (){
                    chrome.tabs.executeScript(null, {
                        file: "YTQ.js"
                    }, function () {});
                });
            });
        }
    }
    chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
        if (changeInfo) {
            setTimeout(function(){
                chrome.tabs.executeScript(null, {file: "YTQ.js"});
            }, 2000); 
        }
    });
}
