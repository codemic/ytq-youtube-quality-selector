# YTQ - YouTube Quality Selector

![YTQ](https://gitlab.com/codemic/ytq-youtube-quality-selector/-/raw/main/screenshots/Layout.png)


Tired of manually selecting the Quality of YouTube videos?
Look no further with a completely private chrome browser plugin to remove the manual labour :)

 